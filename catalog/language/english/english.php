<?php
// Locale
$_['code']                  = 'en';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

// Text
$_['text_home']             = 'Домашняя';
$_['text_yes']              = 'Да';
$_['text_no']               = 'Нет';
$_['text_none']             = ' --- Пусто --- ';
$_['text_select']           = ' --- Выбрать --- ';
$_['text_all_zones']        = 'All Zones';
$_['text_pagination']       = 'Showing {start} to {end} of {total} ({pages} Pages)';
$_['text_separator']        = ' &raquo; ';

// Buttons
$_['button_add_address']    = 'Добавить Адрес';
$_['button_back']           = 'Назад';
$_['button_continue']       = 'Продолжить';
$_['button_cart']           = 'Добавить в корзину';
$_['button_compare']        = 'Добавить в сравнения';
$_['button_wishlist']       = 'Добавить в желаемое';
$_['button_checkout']       = 'Оплата';
$_['button_confirm']        = 'Подтвердить заказ';
$_['button_coupon']         = 'Применить купон';
$_['button_delete']         = 'Удалить';
$_['button_download']       = 'Скачать';
$_['button_edit']           = 'Редактировать';
$_['button_filter']         = 'Уточнить поиск';
$_['button_new_address']    = 'Новый Адрес';
$_['button_change_address'] = 'Изменить Адрес';
$_['button_reviews']        = 'Обзоры';
$_['button_write']          = 'Написать Обзор';
$_['button_login']          = 'Войти';
$_['button_update']         = 'Обновить';
$_['button_remove']         = 'Удалить';
$_['button_reorder']        = 'Заказать занаво';
$_['button_return']         = 'Вернуться';
$_['button_shopping']       = 'Продолжить шоппинг';
$_['button_search']         = 'Поиск';
$_['button_shipping']       = 'Доставить';
$_['button_guest']          = 'Купить как гость';
$_['button_view']           = 'Просмотр';
$_['button_voucher']        = 'Применить Ваучер';
$_['button_upload']         = 'Загрузить Файл';
$_['button_reward']         = 'Apply Points';
$_['button_quote']          = 'Get Quotes';

// Error
$_['error_upload_1']        = 'Warning: The uploaded file exceeds the upload_max_filesize directive in php.ini!';
$_['error_upload_2']        = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';
$_['error_upload_3']        = 'Warning: The uploaded file was only partially uploaded!';
$_['error_upload_4']        = 'Warning: No file was uploaded!';
$_['error_upload_6']        = 'Warning: Missing a temporary folder!';
$_['error_upload_7']        = 'Warning: Failed to write file to disk!';
$_['error_upload_8']        = 'Warning: File upload stopped by extension!';
$_['error_upload_999']      = 'Warning: No error code available!';
?>
