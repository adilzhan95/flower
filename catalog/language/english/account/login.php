<?php
// Heading
$_['heading_title']                = 'Войти в профиль';

// Text
$_['text_account']                 = 'Профиль';
$_['text_login']                   = 'Войти';
$_['text_new_customer']            = 'Новый Пользователь';
$_['text_register']                = 'Создать профиль';
$_['text_register_account']        = 'By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_returning_customer']      = 'Старый пользователь';
$_['text_i_am_returning_customer'] = 'У меня есть профиль';
$_['text_forgotten']               = 'Забыли пароль?';

// Entry
$_['entry_email']                  = 'E-Mail';
$_['entry_password']               = 'Пароль:';

// Error
$_['error_login']                  = 'Внимание! Неверный логин или пароль!';
$_['error_approved']               = 'Внимание! Вам нужно подтвердить профиль перед тем как войти';
?>
