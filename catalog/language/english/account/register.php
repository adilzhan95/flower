<?php
// Heading
$_['heading_title']        = 'Создать Профиль';

// Text
$_['text_account']         = 'Профиль';
$_['text_register']        = 'Зарегестрироваться';
$_['text_account_already'] = 'Если есть профиль, то войдите по <a href="%s">данной ссылке</a>.';
$_['text_your_details']    = 'Ваша личная информация';
$_['text_your_address']    = 'Ваш Адрес';
$_['text_newsletter']      = 'Рассылка';
$_['text_your_password']   = 'Ваш пароль';
$_['text_agree']           = 'I have read and agree to the <a class="colorbox" href="%s" alt="%s"><b>%s</b></a>';

// Entry
$_['entry_firstname']      = 'Имя:';
$_['entry_lastname']       = 'Фамилия:';
$_['entry_email']          = 'E-Mail:';
$_['entry_telephone']      = 'Телефон:';
$_['entry_fax']            = 'Домашний Телефон:';
$_['entry_company']        = 'Компания:';
$_['entry_customer_group'] = 'Вид Бизнеса:';
$_['entry_company_id']     = 'ID Компании:';
$_['entry_tax_id']         = 'ID Налогов:';
$_['entry_address_1']      = 'Адрес 1:';
$_['entry_address_2']      = 'Aдрес 2:';
$_['entry_postcode']       = 'Индекс:';
$_['entry_city']           = 'Город:';
$_['entry_country']        = 'Страна:';
$_['entry_zone']           = 'Регион / Область:';
$_['entry_newsletter']     = 'Подписаться:';
$_['entry_password']       = 'Пароль:';
$_['entry_confirm']        = 'Подтвердить Пароль:';

// Error
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_company_id']     = 'Company ID required!';
$_['error_tax_id']         = 'Tax ID required!';
$_['error_vat']            = 'VAT number is invalid!';
$_['error_address_1']      = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_postcode']       = 'Postcode must be between 2 and 10 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';
$_['error_agree']          = 'Warning: You must agree to the %s!';
?>
