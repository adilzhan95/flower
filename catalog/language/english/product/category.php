<?php
// Text
$_['text_refine']       = 'Улучшить Поиск';
$_['text_product']      = 'Продукция';
$_['text_error']        = 'Категория не найдена!';
$_['text_empty']        = 'There are no products to list in this category.';
$_['text_quantity']     = 'Количество:';
$_['text_manufacturer'] = 'Бренд:';
$_['text_model']        = 'Product Code:';
$_['text_points']       = 'Reward Points:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Ex Tax:';
$_['text_reviews']      = 'Based on %s reviews.';
$_['text_compare']      = 'Сравнить (%s)';
$_['text_display']      = 'Отобразить:';
$_['text_list']         = 'Список';
$_['text_grid']         = 'Сетка';
$_['text_sort']         = 'Фильтр По:';
$_['text_default']      = 'Стандартный';
$_['text_name_asc']     = 'Название (A - А)';
$_['text_name_desc']    = 'Название (Я - A)';
$_['text_price_asc']    = 'Цена (Низ. &gt; Выс.)';
$_['text_price_desc']   = 'Цена (Выс. &gt; Низ.)';
$_['text_rating_asc']   = 'Рейтинг (Низкий)';
$_['text_rating_desc']  = 'Рейтинг (Высокий)';
$_['text_model_asc']    = 'Модель (A - Я)';
$_['text_model_desc']   = 'Модель (Я - A)';
$_['text_limit']        = 'Показать:';
?>
