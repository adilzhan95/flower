<?php
// Text
$_['text_home']           = 'Главная';
$_['text_wishlist']       = 'В корзине (%s)';
$_['text_shopping_cart']  = 'Корзина';
$_['text_search']         = 'Поиск';
$_['text_welcome']        = '<a href="%s">Войти</a> | <a href="%s">Создать Аккаунт</a>.';
$_['text_logged']         = 'Вы зашли <a href="%s">%s</a> <b>(</b> <a href="%s">Выйти</a> <b>)</b>';
$_['text_account']        = 'Мой Аккаунт';
$_['text_checkout']       = 'Оплатить';
?>
