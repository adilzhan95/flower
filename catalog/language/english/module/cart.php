<?php
// Heading
$_['heading_title'] = 'Корзина';

// Text
$_['text_items']    = '%s вещей - %s';
$_['text_empty']    = 'Ваша корзина пуста!';
$_['text_cart']     = 'Посмотреть корзину';
$_['text_checkout'] = 'Покупка';

$_['text_payment_profile'] = 'Payment Profile';
?>
