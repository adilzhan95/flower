<?php
// Heading
$_['heading_title']    = 'Филиал';

// Text
$_['text_register']    = 'Зарегестрироваться';
$_['text_login']       = 'Войти';
$_['text_logout']      = 'Выйти';
$_['text_forgotten']   = 'Забыли Пароль';
$_['text_account']     = 'Мой Профиль';
$_['text_edit']        = 'Редактировать Профиль';
$_['text_password']    = 'Пароль';
$_['text_payment']     = 'Виды Оплаты';
$_['text_tracking']    = 'Остлеживание Филиала';
$_['text_transaction'] = 'Tранзакции';
?>
