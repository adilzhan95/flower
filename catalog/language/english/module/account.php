<?php
// Heading
$_['heading_title']    = 'Профиль';

// Text
$_['text_register']    = 'Зарегестрироваться';
$_['text_login']       = 'Войти';
$_['text_logout']      = 'Выйти';
$_['text_forgotten']   = 'Забыли пароль?';
$_['text_account']     = 'Мой профиль';
$_['text_edit']        = 'Редактировать профиль';
$_['text_password']    = 'Пароль';
$_['text_address']     = 'Адрес';
$_['text_wishlist']    = 'Желаемое';
$_['text_order']       = 'История Заказов';
$_['text_download']    = 'Загрузки';
$_['text_return']      = 'Возвраты';
$_['text_transaction'] = 'Транзакции';
$_['text_newsletter']  = 'Рассылка';
$_['text_recurring']   = 'Оплата';
?>
