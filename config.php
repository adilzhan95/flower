<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/flower/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/flower/');

// DIR
define('DIR_APPLICATION', 'C:\xampp\htdocs\flower/catalog/');
define('DIR_SYSTEM', 'C:\xampp\htdocs\flower/system/');
define('DIR_DATABASE', 'C:\xampp\htdocs\flower/system/database/');
define('DIR_LANGUAGE', 'C:\xampp\htdocs\flower/catalog/language/');
define('DIR_TEMPLATE', 'C:\xampp\htdocs\flower/catalog/view/theme/');
define('DIR_CONFIG', 'C:\xampp\htdocs\flower/system/config/');
define('DIR_IMAGE', 'C:\xampp\htdocs\flower/image/');
define('DIR_CACHE', 'C:\xampp\htdocs\flower/system/cache/');
define('DIR_DOWNLOAD', 'C:\xampp\htdocs\flower/download/');
define('DIR_LOGS', 'C:\xampp\htdocs\flower/system/logs/');

// DB
define('DB_DRIVER', 'mysqliz');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'P83219y7gp');
define('DB_DATABASE', 'flowe');
define('DB_PREFIX', 'oc_');

?>